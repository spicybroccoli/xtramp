<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php
/**
 * Page Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * @package WooFramework
 * @subpackage Template
 */
	get_header();
	global $woo_options;
?>

<div id="content" class="page col-full">
<h1 class="page-title"><?php the_title(); ?></h1>
<ul class="the-tricks">
  <?php /* Start the Loop */ ?>
  <?php 
			query_posts(array('post_parent' => get_the_ID(), 'post_type' => 'page'));

			while ( have_posts() ) : the_post(); $count++; ?>
  
    <li> <a href="<?php the_permalink(); ?>">
    <div class="the-tricks-container">
      <h2 class="title"><?php the_title(); ?></h2>
      <?php the_post_thumbnail(); ?>
      </a> 
      </div></li>
  
  <?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to overload this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					//get_template_part( 'content', get_post_format() );
				?>
  <?php endwhile; ?>
</ul>
</div>
<!-- /#content -->

<?php get_footer(); ?>
