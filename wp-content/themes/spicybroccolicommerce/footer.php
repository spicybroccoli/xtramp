<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php
/**
 * Footer Template
 *
 * Here we setup all logic and XHTML that is required for the footer section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
	global $woo_options;
	
	echo '<div class="footer-wrap">';

	$total = 4;
	if ( isset( $woo_options['woo_footer_sidebars'] ) && ( $woo_options['woo_footer_sidebars'] != '' ) ) {
		$total = $woo_options['woo_footer_sidebars'];
	}

	if ( ( woo_active_sidebar( 'footer-1' ) ||
		   woo_active_sidebar( 'footer-2' ) ||
		   woo_active_sidebar( 'footer-3' ) ||
		   woo_active_sidebar( 'footer-4' ) ) && $total > 0 ) {

?>
	<?php woo_footer_before(); ?>
	
		<section id="footer-widgets" class="col-full col-<?php echo $total; ?> fix">
	
			<?php $i = 0; while ( $i < $total ) { $i++; ?>
				<?php if ( woo_active_sidebar( 'footer-' . $i ) ) { ?>
	
			<div class="block footer-widget-<?php echo $i; ?>">
	        	<?php woo_sidebar( 'footer-' . $i ); ?>
			</div>
	
		        <?php } ?>
			<?php } // End WHILE Loop ?>
	
		</section><!-- /#footer-widgets  -->
	<?php } // End IF Statement ?>
		<section class="footer">
	<div class="container">
    <div class="content">
    	<h3>Stay tuned to XTRAMP</h3>
        <p>Keep up to date with the latest specials, tricks and news</p>
    </div>
    <div class="content">
    <form id="form-footer"><input type="text" placeholder="Email"></form><input type="button" class="btn-footer button" value="Join Us">
    </div>
    <div class="content-divider"></div>
    <div class="content">
        <p>© <?php echo date('Y'); ?> XTRAMP | <a href="<?php echo get_permalink( 12 ); ?>">Terms & Conditions</a> | <a href="<?php echo get_permalink( 14 ); ?>">Postage & Shipping</a></p>
    </div>
    <div class="content sbm">
    Website Design by <a href="http://www.spicybroccoli.com" target="_blank">Spicy Broccoli Media </a>   </div>
    </div>
</section><!-- /#footer  -->
	
	</div><!-- / footer-wrap -->

</div><!-- /#wrapper -->
<?php wp_footer(); ?>
<?php woo_foot(); ?>
</body>
</html>