<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
global $woo_options, $woocommerce;
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php if ( $woo_options['woo_boxed_layout'] == 'true' ) echo 'boxed'; ?> <?php if (!class_exists('woocommerce')) echo 'woocommerce-deactivated'; ?>">
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />

<title><?php woo_title(''); ?></title>
<?php woo_meta(); ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" media="screen" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	wp_head();
	woo_head();
?>

</head>

<body <?php body_class(); ?>>
<?php woo_top(); ?>

<div id="wrapper">
    <?php woo_header_before(); ?>
	<header id="header">
	<div class="container">
    <div id="media-icons">
    	<span class="facebook"><a href="#"></a></span>
        <span class="twitter"><a href="#"></a></span>
        <span class="instagran"><a href="#"></a></span>
        <span class="googleplus"><a href="#"></a></span>
    </div>
    <!-- /#media -->
    	<div id="top">
		<nav role="navigation">
			<?php if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'top-menu' ) ) { ?>
			<?php wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'top-nav', 'menu_class' => 'nav fl', 'theme_location' => 'top-menu' ) ); ?>
			<?php } ?>
			<?php
				if ( class_exists( 'woocommerce' ) ) {
					echo '<ul class="nav wc-nav">';
					woocommerce_cart_link();
					echo '<li class="checkout"><a href="'.esc_url($woocommerce->cart->get_checkout_url()).'">'.__('Checkout','woothemes').'</a></li>';
					echo get_search_form();
					echo '</ul>';
				}
			?>
		</nav>
	</div><!-- /#top -->
    
	    <hgroup>

	    	 <?php $logo = esc_url( get_template_directory_uri() . '/images/logo.png' ); ?>
			    <a id="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr( get_bloginfo( 'description' ) ); ?>">
			    	<img src="<?php echo $logo; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
			    </a>
			<h3 class="nav-toggle"><a href="#navigation"><mark class="websymbols">&#178;</mark> <span><?php _e('Navigation', 'woothemes'); ?></span></a></h3>
		</hgroup>

        <?php woo_nav_before(); ?>

		<nav id="navigation" role="navigation">
			<?php wp_nav_menu( array(
						'menu'			=> 'primary',
						'sort_column'	=> 'menu_order',
						'container' 	=> 'ul', 
						'menu_id' 		=> 'main-nav',
						'menu_class' 	=> 'nav fr'
					) ); ?>
		</nav><!-- /#navigation -->

		<?php woo_nav_after(); ?>
	</div>
	</header><!-- /#header -->
    <?php  if(is_front_page()){ ?>
    <section class="slider">
    <?php  echo do_shortcode("[metaslider id=10]"); ?>
    </section>
<!-- /#slider --> <?php } ?>
	<?php woo_content_before(); ?>