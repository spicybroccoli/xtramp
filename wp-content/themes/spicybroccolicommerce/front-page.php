<?php
/**
 * Page Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * @package WooFramework
 * @subpackage Template
 */
	get_header();
	global $woo_options;
?>
       
    <div class="container">
	<div class="tiles">
    <div class="tiles-row">
    	<div class="tile-container" id="tile-1">
        <a href="http://dev.spicybroccolitesting.com.au/xtramp/product/product-text/">
        	<div class="image-overlay"><img src="http://dev.spicybroccolitesting.com.au/xtramp/wp-content/uploads/2015/03/xtramp.jpg" alt=""/> </div>
            <div class="txt">
            <div class="tile-caret-left"></div>
              <h2>Xtramp Shop</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
        </a>
        </div>
        <div class="tile-container tile-1" id="tile-3">
        <a href="gallery">
            <div class="txt">
            <div class="tile-caret-right"></div>
              <h2>Gallery</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <div class="image-overlay"><img src="http://dev.spicybroccolitesting.com.au/xtramp/wp-content/uploads/2015/03/xtramp.jpg" alt=""/> </div>
       </a>
        </div>
        </div>
        <div class="tiles-row">
        <div class="tile-container tile-1" id="tile-2">
        <a href="#">
        	<div class="image-overlay"><img src="http://dev.spicybroccolitesting.com.au/xtramp/wp-content/uploads/2015/03/xtramp.jpg" alt=""/> </div>
            <div class="txt">
            <div class="tile-caret-left"></div>
              <h2>Videos</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            </a>
        </div>
        <div class="tile-container tile-1" id="tile-4">
        <a href="the-tricks">
            <div class="txt">
            <div class="tile-caret-right"></div>
              <h2>The Tricks</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <div class="image-overlay"><img src="http://dev.spicybroccolitesting.com.au/xtramp/wp-content/uploads/2015/03/xtramp.jpg" alt=""/> </div>
        </div>
        </a>
        </div>
    </div>
</div>
<div class="container">
    <div class="home-about-us">
        <h1>Lorem Ipsum</h1>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
    </div>
</div><!-- /#content -->
		
<?php get_footer(); ?>