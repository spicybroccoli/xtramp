<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="mp-pusher" class="mp-pusher">

    <nav class="mp-menu" id="mp-menu">
		<div class="mp-level">    	
	        <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
	            		'container'  => 'false',
	            		'container_id' => 'mp-menu',
	            		'menu_class' => 'mp-level',
	            		'container_class' => 'mp-menu',
	            		'menu_class' => '',  
	            		'walker' => new SBM_Responsive_Nav()
	            	) 
	            ); 
	        ?>	
                    
        </div>
    </nav>

    <div id="top-menu">
		<a href="#" id="trigger" class="menu-trigger">Open/Close Menu</a>
		<a href="<?php echo home_url(); ?>"><img src="http://placehold.it/180x40"></a>
	</div>

	<div id="wrapper" class="hfeed">
		<div id="header">
			<div id="masthead">
				<div id="branding" role="banner">
					<?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
					<<?php echo $heading_tag; ?> id="site-title">
						<span>
							<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
						</span>
					</<?php echo $heading_tag; ?>>
					<div id="site-description"><?php bloginfo( 'description' ); ?></div>

				</div><!-- #branding -->

				<div id="access" role="navigation">
					<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
				</div><!-- #access -->
			</div><!-- #masthead -->
		</div><!-- #header -->

		<div id="main">
