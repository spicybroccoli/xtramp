<?php
/**
 * Load Our Scripts
 *
 */

class SBM_Landing {

	function __construct() {
		// Use custom html or the existing template
		//$this->htmlLanding();
		//$this->templateLanding();

		add_filter( 'login_redirect', function( $redirect_to, $request, $user  ) {
			return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url();
		}, 10, 3); 


	}

	private function htmlLanding() {
		if (!is_user_logged_in()): ?>
		<html><head><title>Landing Page</title></head>
		<body>
			<div style="width:80px;height:80px;margin:auto auto;position:absolute;top:0;left:0;right:0;bottom:0;">
				<img src="http://commercial.inlite.com.au/wp-content/themes/proggeti/images/logo.jpg">
			</div>
		</body></html>
		<?php exit; endif;
	}

	private function templateLanding() {
		get_header();
		echo '<h2>Landing Page</h2>';
		get_footer();
		exit;
	}


}

new SBM_Branding();