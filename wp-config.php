<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_xtramp');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4IejK{L!Mgri1a-&Un`xfkhNa0LA 0y;!U;*m<m7%3s*E{x#zoc^wVlWx4>R2~@%');
define('SECURE_AUTH_KEY',  'FLoc){@Imych#mCPKurVWQ+Z|DRL_]o)3(}k6/#pr0d0~O*6|KMlJm%8|,c]w@e+');
define('LOGGED_IN_KEY',    '5U>p;o*y2z;7^GM}a{H64=c:`A:HNoR?T=?fZ[ooy;mg[1`[a+4?5>?-P%@]Ckup');
define('NONCE_KEY',        '1G}1Oa$UTDBTXmd(RXx<4oY`+AG@DlFhoI;Mx,/s<Dt%nl*`f 68xf[w9B|PuX{J');
define('AUTH_SALT',        '0#~JYF$aXPfA;ObBJl+-Wy]f*?3CfrrA-_a]3F8vd|uBaU&Ad%|I-[,vZ:AN54Fs');
define('SECURE_AUTH_SALT', 'p*nh%sk+UO$s]96~qQj@UpF2F/,K+$Ao@7`Yxl^dgq*xP--e%3Qj~0FTKKLNAKD3');
define('LOGGED_IN_SALT',   'A%jJ-AL6J)l>Bh7OI|sI9XoB9$Zc%@)4^-;x~t}!0hxop3^9]|J] E@/e!YJh!R]');
define('NONCE_SALT',       '1wE1EI>lRA-yDX-T?`pT&P7Rv*4@-2o5Vz3Njo/~ybd4lJ=/8o513N!]AC-,IHv!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
